import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MainComponent } from './main/main/main.component';
import { ShowAllComponent } from './film_and_director/components/show-all/show-all.component';
import { ShowFilmsComponent } from './film_and_director/components/show-films/show-films.component';
import { ShowAllUsersComponent } from './users/components/show-all/ShowAllUsersComponent';
import { ShowUsersComponent } from './users/components/show-users/show-users.component';
import { UserComponent } from './main/user/user.component';
import { AdminComponent } from './main/admin/admin.component';
import { AddComponent } from './film_and_director/components/addFilm/add.component';
import { DeleteFilmComponent } from './film_and_director/components/delete-film/delete-film.component';
import { UpdateDirectorComponent } from './film_and_director/components/update-director/update-director.component';
import { UpdateTitleComponent } from './film_and_director/components/update-title/update-title.component';
import { AddUserComponent } from './users/components/add-user/add-user.component';
import { DeleteUserComponent } from './users/components/delete-user/delete-user.component';
import { UpdatePasswordComponent } from './users/components/update-password/update-password.component';
import { UpdateRoleComponent } from './users/components/update-role/update-role.component';
import { FailLoginComponent } from './error_pages/fail-login/fail-login.component';
import { AccessDeniedComponent } from './error_pages/access-denied/access-denied.component';
import { FilmRestService } from './rest_services/film_rest.service';
import { UserRestService } from './rest_services/user_rest.service';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    MainComponent,
    UserComponent,
    AdminComponent,
    AddComponent,
    DeleteFilmComponent,
    ShowAllComponent,
    ShowFilmsComponent,
    UpdateDirectorComponent,
    UpdateTitleComponent,
    AddUserComponent,
    DeleteUserComponent,
    ShowAllUsersComponent,
    ShowUsersComponent,
    UpdatePasswordComponent,
    UpdateRoleComponent,
    FailLoginComponent,
    AccessDeniedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [FilmRestService, UserRestService],
  bootstrap: [MainComponent]
})
export class AppModule { }
