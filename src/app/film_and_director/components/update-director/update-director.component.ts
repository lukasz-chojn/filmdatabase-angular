import {Component} from '@angular/core';
import {FilmRestService} from 'src/app/rest_services/film_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-update-director',
  templateUrl: './update-director.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class UpdateDirectorComponent {
  public message = '';
  public tytul: string;
  public rezyser: string;

  constructor(private film: FilmRestService) {
  }

  onSubmit() {
    this.updateDirector();
  }

  updateDirector() {
    this.film.updateDirector(this.tytul, this.rezyser)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
