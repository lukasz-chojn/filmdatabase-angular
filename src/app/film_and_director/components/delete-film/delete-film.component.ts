import {Component} from '@angular/core';
import {FilmRestService} from 'src/app/rest_services/film_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-delete-film',
  templateUrl: './delete-film.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class DeleteFilmComponent {

  tytul = '';
  message = '';

  constructor(private film: FilmRestService) {
  }

  onSubmit() {
    this.deleteFilm();
  }

  deleteFilm() {
    this.film.deleteFilm(this.tytul)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
