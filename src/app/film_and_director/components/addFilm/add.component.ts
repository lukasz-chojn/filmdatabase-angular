import {Component} from '@angular/core';
import {FilmRestService} from 'src/app/rest_services/film_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-add-film',
  templateUrl: './add.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class AddComponent {

  tytul = '';
  rezyser = '';
  czas = '';
  rozmiar = '';
  message = '';

  constructor(private film: FilmRestService) {
  }

  onSubmit() {
    this.addFilm();
  }

  private addFilm() {
    this.film.addFilm(this.tytul, this.rezyser, this.czas, this.rozmiar)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }

}
