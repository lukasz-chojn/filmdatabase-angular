import {HttpErrorResponse} from '@angular/common/http';
import {Component} from '@angular/core';
import {FilmRestService} from 'src/app/rest_services/film_rest.service';

@Component({
  selector: 'app-update-title',
  templateUrl: './update-title.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class UpdateTitleComponent {

  public message = '';
  public oldTitle: string;
  public newTitle: string;

  constructor(private film: FilmRestService) {
  }

  onSubmit() {
    this.updateTitle();
  }

  updateTitle() {
    this.film.updateTitle(this.oldTitle, this.newTitle)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
