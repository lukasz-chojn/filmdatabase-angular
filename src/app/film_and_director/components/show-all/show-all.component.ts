import {Component, OnInit} from '@angular/core';
import {FilmRestService} from './../../../rest_services/film_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-show-all-films',
  templateUrl: './show-all.component.html',
  styleUrls: ['../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class ShowAllComponent implements OnInit {
  public result: any[] = [];
  public message: string;

  constructor(private films: FilmRestService) {
  }

  ngOnInit(): void {
    this.showAll();
  }

  showAll() {
    this.films.getAllFilms()
      .subscribe(
        (data: any[]) => {
          this.result = data;
          if (this.result.length === 0) {
            this.message = 'Brak filmów';
          }
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
