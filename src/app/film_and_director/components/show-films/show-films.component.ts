import {Component} from '@angular/core';
import {FilmRestService} from 'src/app/rest_services/film_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-show-films',
  templateUrl: './show-films.component.html',
  styleUrls: ['../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class ShowFilmsComponent {

  constructor(private film: FilmRestService) {
  }

  public result: any[] = [];
  public tytul: string;
  public message: string;

  onSubmit() {
    this.getFilmByTitle();
  }

  getFilmByTitle() {
    this.film.getAllFilmsByTitle(this.tytul)
      .subscribe(
        (data: any[]) => {
          this.result = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
