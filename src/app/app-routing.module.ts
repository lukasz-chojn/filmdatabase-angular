import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main/main.component';
import { ShowAllComponent } from './film_and_director/components/show-all/show-all.component';
import { ShowFilmsComponent } from './film_and_director/components/show-films/show-films.component';
import { ShowAllUsersComponent } from './users/components/show-all/ShowAllUsersComponent';
import { ShowUsersComponent } from './users/components/show-users/show-users.component';
import { UserComponent } from './main/user/user.component';
import { AdminComponent } from './main/admin/admin.component';
import { AddComponent } from './film_and_director/components/addFilm/add.component';
import { DeleteFilmComponent } from './film_and_director/components/delete-film/delete-film.component';
import { UpdateDirectorComponent } from './film_and_director/components/update-director/update-director.component';
import { UpdateTitleComponent } from './film_and_director/components/update-title/update-title.component';
import { AddUserComponent } from './users/components/add-user/add-user.component';
import { DeleteUserComponent } from './users/components/delete-user/delete-user.component';
import { UpdatePasswordComponent } from './users/components/update-password/update-password.component';
import { UpdateRoleComponent } from './users/components/update-role/update-role.component';
import { FailLoginComponent } from './error_pages/fail-login/fail-login.component';
import { AccessDeniedComponent } from './error_pages/access-denied/access-denied.component';

const routes: Routes = [
  {
    path: 'home', component: MainComponent
  },
  {
    path: 'addfilm', component: AddComponent
  },
  {
    path: 'deletefilm', component: DeleteFilmComponent
  },
  {
    path: 'allfilms', component: ShowAllComponent
  },
  {
    path: 'allfilmsbytitle', component: ShowFilmsComponent
  },
  {
    path: 'updatedirector', component: UpdateDirectorComponent
  },
  {
    path: 'updatetitle', component: UpdateTitleComponent
  },
  {
    path: 'adduser', component: AddUserComponent
  },
  {
    path: 'deleteuser', component: DeleteUserComponent
  },
  {
    path: 'allusers', component: ShowAllUsersComponent
  },
  {
    path: 'allusersbyusername', component: ShowUsersComponent
  },
  {
    path: 'updatepassword', component: UpdatePasswordComponent
  },
  {
    path: 'updaterole', component: UpdateRoleComponent
  },
  {
    path: 'user', component: UserComponent
  },
  {
    path: 'admin', component: AdminComponent
  },
  {
    path: 'failLogin', component: FailLoginComponent
  },
  {
    path: 'accessDenied', component: AccessDeniedComponent
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, { anchorScrolling: 'enabled' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
