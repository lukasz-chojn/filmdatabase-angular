import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['../../css/bootstrap.min.css', '../../css/style.css']
})
export class MainComponent {
  title = 'films-database-angular';
}
