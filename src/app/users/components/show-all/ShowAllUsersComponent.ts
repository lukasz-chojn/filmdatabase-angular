import {Component, OnInit} from '@angular/core';
import {UserRestService} from 'src/app/rest_services/user_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-show-all-users',
  templateUrl: './show-all.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class ShowAllUsersComponent implements OnInit {
  constructor(private user: UserRestService) {
  }

  public users: any[] = [];
  public message = '';

  ngOnInit() {
    this.showAllUsers();
  }

  showAllUsers() {
    this.user.getAllUsers()
      .subscribe(
        (data: any[]) => {
          if (data.length === 0) {
            this.message = 'Baza użytkowników jest pusta';
            return;
          }
          this.users = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
