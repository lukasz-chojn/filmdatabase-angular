import {Component} from '@angular/core';
import {UserRestService} from 'src/app/rest_services/user_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class UpdatePasswordComponent {

  constructor(private user: UserRestService) {
  }

  public newpass = '';
  public username = '';
  public confirmpass = '';
  public message = '';
  public isSelected: boolean;

  onSubmit() {
    this.updatePass();
  }

  private updatePass() {
    if (this.newpass !== this.confirmpass || !this.isSelected === true) {
      this.message = 'Hasła nie są zgodne';
      return;
    }
    this.user.updatePass(this.newpass, this.username)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
