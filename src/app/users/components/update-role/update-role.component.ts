import {Component} from '@angular/core';
import {UserRestService} from 'src/app/rest_services/user_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-update-role',
  templateUrl: './update-role.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class UpdateRoleComponent {

  constructor(private userService: UserRestService) {
  }

  public newrole = '';
  public user = '';
  public message = '';

  onSubmit() {
    this.updateRole();
  }

  private updateRole() {
    this.userService.updateRole(this.newrole, this.user)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
