import {Component} from '@angular/core';
import {UserRestService} from 'src/app/rest_services/user_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class AddUserComponent {

  constructor(private user: UserRestService) {
  }

  public password = '';
  public confirmpass = '';
  public username = '';
  public role = '';
  public message = '';

  onSubmit() {
    this.addUser();
  }

  private addUser() {
    if (this.password !== this.confirmpass) {
      this.message = 'Hasła nie są zgodne';
      return;
    }
    if (this.username === null) {
      this.message = 'Login użytkownika nie może być pusty';
      return;
    }
    this.user.addUser(this.username, this.password, this.confirmpass, this.role)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );

  }

}
