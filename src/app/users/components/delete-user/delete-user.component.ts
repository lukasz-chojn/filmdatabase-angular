import {Component} from '@angular/core';
import {UserRestService} from 'src/app/rest_services/user_rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class DeleteUserComponent {

  constructor(private user: UserRestService) {
  }

  public message = '';
  public username = '';

  onSubmit() {
    this.deleteUser();
  }

  private deleteUser() {
    if (this.username === null || typeof this.username === 'undefined') {
      this.message = 'Login nie może być pusty';
      return;
    }
    this.user.deleteUser(this.username)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
