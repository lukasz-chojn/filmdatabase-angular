import {Component} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {UserRestService} from 'src/app/rest_services/user_rest.service';

@Component({
  selector: 'app-show-users',
  templateUrl: './show-users.component.html',
  styleUrls: ['../../../css/bootstrap-form.css', '../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class ShowUsersComponent {

  constructor(private user: UserRestService) {
  }

  public users: any[] = [];
  public username = '';
  public message = '';

  onSubmit() {
    this.showAllUsersByUsername();
  }

  showAllUsersByUsername() {
    this.user.getAllUsersByUsername(this.username)
      .subscribe(
        (data: any[]) => {
          if (data.length === 0) {
            this.message = 'Brak użytkowników w bazie o wskazanym loginie';
            return;
          }
          this.users = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
