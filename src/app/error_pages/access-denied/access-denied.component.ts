import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['../css/style_error.css', '../css/font-style.css', '../css/font-awesome.min.css']
})
export class AccessDeniedComponent {

  constructor() {
  }

}
