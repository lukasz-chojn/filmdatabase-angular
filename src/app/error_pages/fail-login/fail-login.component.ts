import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-fail-login',
  templateUrl: './fail-login.component.html',
  styleUrls: ['../css/style_error.css', '../css/font-style.css', '../css/font-awesome.min.css']
})
export class FailLoginComponent {

  constructor() {
  }
}
