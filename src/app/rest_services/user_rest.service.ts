import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserRestService {

  mainPartOfLink = 'http://localhost:8000/Films/';
  showAllOrPostLink = this.mainPartOfLink + 'users';
  showUsersByUsernameLink = this.mainPartOfLink + 'users/user';
  updateRoleLink = this.mainPartOfLink + 'users/updaterole';
  updatePassLink = this.mainPartOfLink + 'users/updatepass';
  deleteUserLink = this.mainPartOfLink + 'users/delete';

  headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    Accept: ['application/json', 'application/x-www-form-urlencoded', 'text/plain'],
  };
  httpHeaders = {
    headers: new HttpHeaders(this.headers)
  };

  private body: any;

  constructor(private http: HttpClient) {
  }

  addUser(username: string, password: string, confirmpass: string, role: string): Observable<any> {
    return this.http.post<any>(this.showAllOrPostLink + `?user=${username}&password=${password}&confirmPassword=${confirmpass}&role=${role}`, this.body, this.httpHeaders);
  }

  getAllUsers(): Observable<any> {
    return this.http.get<any>(this.showAllOrPostLink);
  }

  getAllUsersByUsername(username: string): Observable<any> {
    return this.http.get<any>(this.showUsersByUsernameLink + `?user=${username}`, this.httpHeaders);
  }

  updateRole(newrole: string, user: string): Observable<any> {
    return this.http.put<any>(this.updateRoleLink + `?newrole=${newrole}&user=${user}`, this.body, this.httpHeaders);
  }

  updatePass(newpass: string, username: string): Observable<any> {
    return this.http.put<any>(this.updatePassLink + `?newpass=${newpass}&user=${username}`, this.body, this.httpHeaders);
  }

  deleteUser(username: string): Observable<any> {
    return this.http.delete<any>(this.deleteUserLink + `/${username}`, this.httpHeaders);
  }
}
