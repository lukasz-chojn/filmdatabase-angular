import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilmRestService {

  mainPartOfLink = 'http://localhost:8000/Films/';
  showAllOrPostLink = this.mainPartOfLink + 'film';
  showFilmsByTitleLink = this.mainPartOfLink + 'film/tytul';
  updateTitleLink = this.mainPartOfLink + 'film/update';
  deleteFilmLink = this.mainPartOfLink + 'film/delete';
  updateDirectorLink = this.mainPartOfLink + 'director/update';

  headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    Accept: ['application/json', 'application/x-www-form-urlencoded', 'text/plain'],
  };
  httpHeaders = {
    headers: new HttpHeaders(this.headers)
  };
  private body: any;

  constructor(private http: HttpClient) {
  }

  addFilm(tytul: string, rezyser: string, czas: string, rozmiar: string): Observable<any> {
    // tslint:disable-next-line: max-line-length
    return this.http.post<any>(this.showAllOrPostLink + `?tytul=${tytul}&rezyser=${rezyser}&czas=${czas}&rozmiar=${rozmiar}`, this.body, this.httpHeaders);
  }

  getAllFilms(): Observable<any> {
    return this.http.get<any>(this.showAllOrPostLink);
  }

  getAllFilmsByTitle(tytul: string): Observable<any> {
    return this.http.get<any>(this.showFilmsByTitleLink + `?tytul=${tytul}`, this.httpHeaders);
  }

  updateTitle(oldTitle: string, newTitle: string): Observable<any> {
    return this.http.put<any>(this.updateTitleLink + `?oldTitle=${oldTitle}&newTitle=${newTitle}`, this.body, this.httpHeaders);
  }

  updateDirector(title: string, director: string): Observable<any> {
    return this.http.put<any>(this.updateDirectorLink + `?title=${title}&director=${director}`, this.body, this.httpHeaders);
  }

  deleteFilm(title: string): Observable<any> {
    return this.http.delete<any>(this.deleteFilmLink + `/${title}`, this.httpHeaders);
  }
}
